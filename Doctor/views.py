from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def dummy(request):
    return HttpResponse('Hello doctor')

def doctor_view(request):
    return render(request,'doctor_view.html')