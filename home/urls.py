from django.urls import path
from home.views import *


urlpatterns = [
    path('',home,name='home'),
    path('login/',user_login,name='login'),
    path('logout/',log_out,name='logout'),
    path('about/',about,name='about'),
]