from django.shortcuts import render
from django.contrib.auth import authenticate,login,logout
from .models import *
from django.shortcuts import redirect

def home(request):
    return render(request,'home.html')

def user_login(request):
    if request.method == "POST":
        email = request.POST['email']
        password = request.POST['password']
        
        user = authenticate(request,email=email,password=password)
        print ('view2',user)
        if user is not None and user.is_admin:
            login(request,user)
          
            return redirect ('admin_view')
        elif user is not None and user.is_doctor:
            login(request,user)
            return redirect('doctor_view')
        elif user is not None and user.is_patient:
            login(request,user)
            return redirect('patient_view')
        else:
            
            return render (request,'login.html')
           
        
    else:
         return render(request,'login.html')

def log_out(request):
    logout (request)
    return render (request,'login.html')

def about(request):
    return render(request,'about.html')