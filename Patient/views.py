from django.shortcuts import render,redirect
from django.http import HttpResponse
from Appointment.models import *
from django.contrib import messages
from django.contrib.auth.hashers import make_password
import random
from django.template.loader import render_to_string
from django.core.mail import EmailMessage
from django.conf import settings
import datetime



# Create your views here.
def patient(request):
    return render(request,'homes.html')

def patient_view(request):
    return render(request,'patient_view.html')


def signup(request):
    if request.method == 'POST':
        name = request.POST['name']
        email = request.POST['email']
        
        password = request.POST['password']
        # Generating password 
        
        print ('view1',password)

        # password hashing
        # password = make_password(request.POST['password'])
        hash_password = make_password(password)

        # patient = Patient(name=name,email=email,contact=contact,address=address)
        # patient.save()
        # messages.success(request,'Details saved successfully')
        account = Accounts(email=email,password=hash_password,is_patient=True)
        account.save()
        

        # saving the details in patient model using account id
        create_patient(request,account.pk)
        
    


        # sending login credentials to the patient
        credentials = {'email':email,'password':password,'name':name}
        html_template = 'login_credential.html'
        html_message =  render_to_string(html_template,context=credentials)
        subject = 'Welcome to Medinova Hospitals'
        email_from = settings.EMAIL_HOST_USER
       
        recipient_list=[email]
        message = EmailMessage(subject,html_message,email_from,recipient_list)
        message.content_subtype = 'html'
        message.send()
        messages.success(request,'Details saved successfully')

      
        
    return render(request,'signup.html')



def create_patient(request,account_id):
    
    if request.method == 'POST':
        name = request.POST['name']
        email = request.POST['email']
        contact = request.POST['contact']
        address = request.POST['address']
        
        patient = Patient(name=name,email=email,contact=contact,address=address,account_id=account_id)
        patient.save()



def doctor_list(request):
    user_id = bool(request.user.is_patient)
    
    print ('moooney',user_id)
    doc = Doctor.objects.all()
    
    slot = Slots.objects.all()
    
    
        
    return render (request,'doctor_list.html',{'doctor':doc,'slot':slot})

def book_appointment(request,id):
    user_id = request.user.id
    pa = Patient.objects.get(account_id = user_id)
    pi = Doctor.objects.get(pk=id)
    
    slot = Slots.objects.filter(doctor_id = id)
    
    if request.method == 'POST':
        patient = pa.id
        hospital = request.POST['hospital']
        doctor = pi.id
        date = request.POST['date']
        slot = request.POST['slot']

        appointment = Appointment(patient_id= patient,hospital_id=hospital,doctor_id=doctor,date=date,slot_id=slot)
        appointment.save()
        messages.success(request,'Appointment booked successfully')
    return render(request,'book_appointment.html',{'pi':pi,'slot':slot,'pa':pa})


def load_doctor(request):
    hospital_id = request.GET.get('hospital')
    doctor = Doctor.objects.filter(hospital_id=hospital_id).order_by('name')
    return render(request,'load_doctor.html',{'doctor':doctor})









    